{% extends template.parent.name %}
{% block content %}
Dear customer,

Our automated DNSSEC-key management system has detected a configuration of DNS servers and CDNSKEY records which does not meet the conditions for the automated DNSSEC-key management. Unless you change the configuration, your {{ domain }} domain will not be included in the automated DNSSEC-key management.

Detected at: {{ datetime }}

However, in compliance with RFC 7344 and RFC 8078, your DNS servers still will be checked regularly. Once we detect a suitable configuration of CDNSKEY records, we will initiate the transition of your domain to the automated DNSSEC-key management mode.

Yours sincerely
Support of CZ.NIC, the CZ domain registry
{% endblock %}
