{% extends template.parent.name %}
{% block content %}
=====================================================================
Notification of changes 
=====================================================================
{% if type == 3 %}Domain{% elif type == 1 %}Contact{% elif type == 2 %}NS set{% elif type == 4 %}keyset{% endif %} data change 
{% if type == 3 %}Domain{% elif type == 1 %}Contact{% elif type == 2 %}NS set{% elif type == 4 %}keyset{% endif %} handle : {{ handle }}
Ticket :  {{ ticket }}
Registrar : {{ registrar }}
=====================================================================

Dear customer,
 
The request was processed successfully, {% if changes %}the required changes have been applied{% else %}no changes were requested, the data remains the same{% endif %}.

{% if changes %}
Original values:
=====================================================================
{%   if changes.object.authinfo %}Authinfo: private value
{%   endif %}
{%   if type == 1 %}{%     if changes.contact.name %}Name: {{ changes.contact.name.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.org %}Organization: {{ changes.contact.org.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.address.permanent %}Permanent Address: {{ changes.contact.address.permanent.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.address.mailing %}Mailing address: {{ changes.contact.address.mailing.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.address.billing %}Billing address: {{ changes.contact.address.billing.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.address.shipping %}Shipping address: {{ changes.contact.address.shipping.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.address.shipping_2 %}Shipping address: {{ changes.contact.address.shipping_2.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.address.shipping_3 %}Shipping address: {{ changes.contact.address.shipping_3.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.telephone %}Telephone: {{ changes.contact.telephone.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.fax %}Fax: {{ changes.contact.fax.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.email %}Email: {{ changes.contact.email.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.notify_email %}Notification email: {{ changes.contact.notify_email.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.ident_type %}Identification type: {{ changes.contact.ident_type.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.ident %}Identification data: {{ changes.contact.ident.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.vat %}VAT number: {{ changes.contact.vat.old|default:"value not set" }}
{%     endif %}{%     if changes.contact.disclose %}
Data visibility:
{%       if changes.contact.disclose.name %}  Name: {% if changes.contact.disclose.name.old %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.org %}  Organization: {% if changes.contact.disclose.org.old %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.email %}  Email: {% if changes.contact.disclose.email.old %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.address %}  Address: {% if changes.contact.disclose.address.old %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.notify_email %}  Notification email: {% if changes.contact.disclose.notify_email.old %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.ident %}  Identification data: {% if changes.contact.disclose.ident.old %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.vat %}  VAT number: {% if changes.contact.disclose.vat.old %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.telephone %}  Telephone: {% if changes.contact.disclose.telephone.old %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.fax %}  Fax: {% if changes.contact.disclose.fax.old %}public{% else %}hidden{% endif %}{% endif %}
{%     endif %}
{%   elif type == 2 %}{%     if changes.nsset.check_level %}Check level: {{ changes.nsset.check_level.old|default:"value not set" }}
{%     endif %}{%     if changes.nsset.tech_c %}Technical contacts: {{ changes.nsset.tech_c.old|default:"value not set" }}
{%     endif %}{%     for item in changes.nsset.dns.old %}Name server: {{ item }}
{%     endfor %}
{%   elif type == 3 %}{%     if changes.domain.registrant %}Holder: {{ changes.domain.registrant.old|default:"value not set" }}
{%     endif %}{%     if changes.domain.nsset %}Name server set: {{ changes.domain.nsset.old|default:"value not set" }}
{%     endif %}{%     if changes.domain.keyset %}Key set: {{ changes.domain.keyset.old|default:"value not set" }}
{%     endif %}{%     if changes.domain.admin_c %}Administrative contacts: {{ changes.domain.admin_c.old|default:"value not set" }}
{%     endif %}{%     if changes.domain.temp_c %}Temporary contacts: {{ changes.domain.temp_c.old|default:"value not set" }}
{%     endif %}{%     if changes.domain.val_ex_date %}Validation expiration date: {{ changes.domain.val_ex_date.old|default:"value not set" }}
{%     endif %}{%     if changes.domain.publish %}Include in ENUM dict: {% if changes.domain.publish.old %}yes{% else %}no{% endif %}{% endif %}
{%   elif type == 4 %}{%     if changes.keyset.tech_c %}Technical contacts: {{ changes.keyset.tech_c.old|default:"value not set" }}
{%     endif %}{%     for item in changes.keyset.dnskey.old %}DNS keys: {{ item }}
{%     endfor %}
{%   endif %}
New values:
=====================================================================
{%   if changes.object.authinfo %}Authinfo: value was changed
{%   endif %}
{%   if type == 1 %}{%     if changes.contact.name %}Name: {{ changes.contact.name.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.org %}Organization: {{ changes.contact.org.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.address.permanent %}Permanent Address: {{ changes.contact.address.permanent.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.address.mailing %}Mailing address: {{ changes.contact.address.mailing.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.address.billing %}Billing address: {{ changes.contact.address.billing.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.address.shipping %}Shipping address: {{ changes.contact.address.shipping.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.address.shipping_2 %}Shipping address: {{ changes.contact.address.shipping_2.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.address.shipping_3 %}Shipping address: {{ changes.contact.address.shipping_3.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.telephone %}Telephone: {{ changes.contact.telephone.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.fax %}Fax: {{ changes.contact.fax.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.email %}Email: {{ changes.contact.email.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.notify_email %}Notification email: {{ changes.contact.notify_email.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.ident_type %}Identification type: {{ changes.contact.ident_type.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.ident %}Identification data: {{ changes.contact.ident.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.vat %}VAT number: {{ changes.contact.vat.new|default:"value deleted" }}
{%     endif %}{%     if changes.contact.disclose %}
Data visibility:
{%       if changes.contact.disclose.name %}  Name: {% if changes.contact.disclose.name.new %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.org %}  Organization: {% if changes.contact.disclose.org.new %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.email %}  Email: {% if changes.contact.disclose.email.new %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.address %}  Address: {% if changes.contact.disclose.address.new %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.notify_email %}  Notification email: {% if changes.contact.disclose.notify_email.new %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.ident %}  Identification data: {% if changes.contact.disclose.ident.new %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.vat %}  VAT number: {% if changes.contact.disclose.vat.new %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.telephone %}  Telephone: {% if changes.contact.disclose.telephone.new %}public{% else %}hidden{% endif %}
{%       endif %}{%       if changes.contact.disclose.fax %}  Fax: {% if changes.contact.disclose.fax.new %}public{% else %}hidden{% endif %}{% endif %}
{%     endif %}
{%   elif type == 2 %}{%     if changes.nsset.check_level %}Check level: {{ changes.nsset.check_level.new|default:"value deleted" }}
{%     endif %}{%     if changes.nsset.tech_c %}Technical contacts: {{ changes.nsset.tech_c.new|default:"value deleted" }}
{%     endif %}{%     for item in changes.nsset.dns.new %}Name server: {{ item }}
{%     endfor %}
{%   elif type == 3 %}{%     if changes.domain.registrant %}Holder: {{ changes.domain.registrant.new|default:"value deleted" }}
{%     endif %}{%     if changes.domain.nsset %}Name server set: {{ changes.domain.nsset.new|default:"value deleted" }}
{%     endif %}{%     if changes.domain.keyset %}Key set: {{ changes.domain.keyset.new|default:"value deleted" }}
{%     endif %}{%     if changes.domain.admin_c %}Administrative contacts: {{ changes.domain.admin_c.new|default:"value deleted" }}
{%     endif %}{%     if changes.domain.temp_c %}Temporary contacts: {{ changes.domain.temp_c.new|default:"value deleted" }}
{%     endif %}{%     if changes.domain.val_ex_date %}Validation expiration date: {{ changes.domain.val_ex_date.new|default:"value deleted" }}
{%     endif %}{%     if changes.domain.publish %}Include in ENUM dict: {% if changes.domain.publish.new %}yes{% else %}no{% endif %}{% endif %}
{%   elif type == 4 %}{%     if changes.keyset.tech_c %}Technical contacts: {{ changes.keyset.tech_c.new|default:"value deleted" }}
{%     endif %}{%     for item in changes.keyset.dnskey.new %}DNS keys: {{ item }}
{%     endfor %}
{%   endif %}
Other data has not been modified.
{% endif %}


The full details of {% if type == 3 %}domain{% elif type == 1 %}contact{% elif type == 2 %}nsset{% elif type == 4 %}keyset{% endif %} can be seen at https://www.nic.cz/whois/{% if type == 3 %}domain{% elif type == 1 %}contact{% elif type == 2 %}nsset{% elif type == 4 %}keyset{% endif %}/{{ handle }}/

In case of any questions please contact your designated registrar
which performed the changes.

Yours sincerely
Support of CZ.NIC, the CZ domain registry
{% endblock %}
