{% extends template.parent.name %}
{% block content %}
======================================================================
Notification of registration
======================================================================
{% if type == 3 %}Domain{% elif type == 1 %}Contact{% elif type == 2 %}NS set{% elif type == 4 %}Keyset{% endif %} creation
{% if type == 3 %}Domain{% elif type == 1 %}Contact{% elif type == 2 %}NS set{% elif type == 4 %}Keyset{% endif %} handle : {{ handle }}
Ticket :  {{ ticket }}
Registrar : {{ registrar }}
======================================================================

Dear customer,

The request was processed successfully, the required registration
has been performed.
{% if type == 3 %}

We recommend updating domain data in the registry after every change
to avoid possible problems with domain renewal or with domain manipulation
done by persons who are not authorized anymore.
{% endif %}
{% if type != 1 %}
Details of {% if type == 3 %}domain{% elif type == 2 %}nsset{% elif type == 4 %}keyset{% endif %} can be seen at https://www.nic.cz/whois/{% if type == 3 %}domain{% elif type == 2 %}nsset{% elif type == 4 %}keyset{% endif %}/{{ handle }}/
{% else %}
Details of the contact are:

{%   if fresh.object.authinfo %}Authinfo: private value
{%   endif %}{%   if fresh.contact.name %}Name: {{ fresh.contact.name }}
{%   endif %}{%   if fresh.contact.org %}Organization: {{ fresh.contact.org }}
{%   endif %}{%   if fresh.contact.address.permanent %}Permanent Address: {{ fresh.contact.address.permanent }}
{%   endif %}{%   if fresh.contact.address.mailing %}Mailing address: {{ fresh.contact.address.mailing }}
{%   endif %}{%   if fresh.contact.address.billing %}Billing address: {{ fresh.contact.address.billing }}
{%   endif %}{%   if fresh.contact.address.shipping %}Shipping address: {{ fresh.contact.address.shipping }}
{%   endif %}{%   if fresh.contact.address.shipping_2 %}Shipping address: {{ fresh.contact.address.shipping_2 }}
{%   endif %}{%   if fresh.contact.address.shipping_3 %}Shipping address: {{ fresh.contact.address.shipping_3 }}
{%   endif %}{%   if fresh.contact.telephone %}Telephone: {{ fresh.contact.telephone }}
{%   endif %}{%   if fresh.contact.fax %}Fax: {{ fresh.contact.fax }}
{%   endif %}{%   if fresh.contact.email %}Email: {{ fresh.contact.email }}
{%   endif %}{%   if fresh.contact.notify_email %}Notification email: {{ fresh.contact.notify_email }}
{%   endif %}{%   if fresh.contact.ident_type %}Identification type: {{ fresh.contact.ident_type }}
{%   endif %}{%   if fresh.contact.ident %}Identification data: {{ fresh.contact.ident }}
{%   endif %}{%   if fresh.contact.vat %}VAT number: {{ fresh.contact.vat }}
{%   endif %}
{%   if fresh.contact.disclose %}Data visibility:
{%     if fresh.contact.disclose.name %}  Name: public
{%     endif %}{%     if fresh.contact.disclose.org %}  Organization: public
{%     endif %}{%     if fresh.contact.disclose.email %}  Email: public
{%     endif %}{%     if fresh.contact.disclose.address %}  Address: public
{%     endif %}{%     if fresh.contact.disclose.notify_email %}  Notification email: public
{%     endif %}{%     if fresh.contact.disclose.ident %}  Identification data: public
{%     endif %}{%     if fresh.contact.disclose.vat %}  VAT number: public
{%     endif %}{%     if fresh.contact.disclose.telephone %}  Telephone: public
{%     endif %}{%     if fresh.contact.disclose.fax %}  Fax: public
{%     endif %}
{%   endif %}
{% endif %}
Yours sincerely
Support of CZ.NIC, the CZ domain registry
{% endblock %}
