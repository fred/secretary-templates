{% extends template.parent.name %}
{% load i18n parsers %}
{% block content %}
Dear customer,

Based on your request submitted via the web form on our pages on
{{ reqdate }}, which was assigned
the identification number {{ reqid }}, we are sending you the requested
password belonging to the {% if type == 3 %}domain name{% elif type == 1 %}contact identified with{% elif type == 2 %}NS set identified with{% elif type == 4 %}keyset identified with{% endif %} {{ handle }}.

The password is: {{ authinfo }}
{% language 'en' %}{% if authinfo_expiration %}The password expires on: {{ authinfo_expiration|parse_datetime|date:"SHORT_DATETIME_FORMAT" }}{% else %}The password is valid for 14 days after its creation.{% endif %}{% endlanguage %}

If you did not submit the aforementioned request, please notify us of
this fact at the address podpora@nic.cz.

Yours sincerely
Support of CZ.NIC, the CZ domain registry
{% endblock %}
