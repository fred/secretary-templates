{% extends template.parent.name %}
{% block content %}
Dear customer,

With regard to the fact that the {% if type == 1 %}contact{% elif type == 2 %}NS set{% elif type == 4 %}keyset{% endif %} identified with {{ handle }}
was not used during the determined period, i.e. attached to a domain{% if type == 1 %} or used as mojeID account{% endif %}, we are
cancelling the aforementioned {% if type == 1 %}contact{% elif type == 2 %}set of nameservers{% elif type == 4 %}set of keysets{% endif %} as of {{ deldate }}.

Cancellation of the {% if type == 1 %}contact{% elif type == 2 %}NS set{% elif type == 4 %}keyset{% endif %} has no influence on functionality of your
registred domains, since it is impossible to cancel a {% if type == 1 %}contact{% elif type == 2 %}NS set{% elif type == 4 %}keyset{% endif %} assigned to a domain.

Yours sincerely
Support of CZ.NIC, the CZ domain registry
{% endblock %}
