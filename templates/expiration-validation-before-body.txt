{% extends template.parent.name %}
{% block content %}
Dear customer,

We would like to notify you that as of {{ checkdate }},
the {{ domain }} domain name validation has not been extended.
The validation will expire on {{ valdate }}. If you plan to renew validation
of the aforementioned domain name, please, contact your registrar, and
perform the extension of validation of your domain name together before
this date.

At present, we keep the following details concerning your domain:

Domain name: {{ domain }}
Owner: {{ owner }}
Registrar: {{ registrar }}
{% for admin in administrators %}{% spaceless %}
Admin contact: {{ admin }}
{% endspaceless %}
{% endfor %}
Yours sincerely
Support of CZ.NIC, the CZ domain registry
{% endblock %}
