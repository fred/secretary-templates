{% extends "b9885448-5dc5-4234-938c-034099971d29.email-base.txt" %}
{% block content %}
NS set: {{ handle }}

Date of the check: {{ checkdate }}
Check type: periodic
Ticket: {{ ticket }}


=== Errors ==================================================
{% for test in tests %}{%   if test.type == "error" %}{%     if test.name == "glue_ok" %}
The required glue record is missing for the following nameservers:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "existence" %}
Following nameservers in the NS set are unreachable:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "autonomous" %}
The NS set does not contain at least two nameservers in different autonomous systems.
{%     elif test.name == "presence" %}
{%       for ns in test.ns %}
Nameserver {{ ns.hostname }} does not contain a record for any of the domains:
{%         for fqdn in ns.fqdn %}    {{ fqdn }}
{%         endfor %}{%         if ns.overfull %}    ...{% endif %}
{%       endfor %}
{%     elif test.name == "authoritative" %}
{%       for ns in test.ns %}
Nameserver {{ ns.hostname }} is not authoritative for domains:
{%         for fqdn in ns.fqdn %}    {{ fqdn }}
{%         endfor %}{%         if ns.overfull %}    ...{% endif %}
{%       endfor %}
{%     elif test.name == "heterogenous" %}
All nameservers in the NS set use the same implementation of DNS server.
{%     elif test.name == "notrecursive" %}
Following nameservers in the NS set are recursive:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "notrecursive4all" %}
Following nameservers in the NS set answered a query recursively:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "dnsseckeychase" %}
For the following domains belonging to the NS set, the DNSSEC signature
could not be validated:
{%       for domain in test.ns %}    {{ domain }}
{%       endfor %}
{%     endif %}{%   endif %}{% endfor %}
=== Warnings =============================================
{% for test in tests %}{%   if test.type == "warning" %}{%     if test.name == "glue_ok" %}
The required glue record is missing for the following nameservers:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "existence" %}
Following nameservers in the NS set are unreachable:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "autonomous" %}
The NS set does not contain at least two nameservers in different autonomous systems.
{%     elif test.name == "presence" %}
{%       for ns in test.ns %}
Nameserver {{ ns.hostname }} does not contain a record for any of the domains:
{%         for fqdn in ns.fqdn %}    {{ fqdn }}
{%         endfor %}{%         if ns.overfull %}    ...{% endif %}
{%       endfor %}
{%     elif test.name == "authoritative" %}
{%       for ns in test.ns %}
Nameserver {{ ns.hostname }} is not authoritative for domains:
{%         for fqdn in ns.fqdn %}    {{ fqdn }}
{%         endfor %}{%         if ns.overfull %}    ...{% endif %}
{%       endfor %}
{%     elif test.name == "heterogenous" %}
All nameservers in the NS set use the same implementation of DNS server.
{%     elif test.name == "notrecursive" %}
Following nameservers in the NS set are recursive:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "notrecursive4all" %}
Following nameservers in the NS set answered a query recursively:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "dnsseckeychase" %}
For the following domains belonging to the NS set, the DNSSEC signature
could not be validated:
{%       for domain in test.ns %}    {{ domain }}
{%       endfor %}
{%     endif %}{%   endif %}{% endfor %}
=== Notices ============================================
{% for test in tests %}{%   if test.type == "notice" %}{%     if test.name == "glue_ok" %}
The required glue record is missing for the following nameservers:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "existence" %}
Following nameservers in the NS set are unreachable:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "autonomous" %}
The NS set does not contain at least two nameservers in different autonomous systems.
{%     elif test.name == "presence" %}
{%       for ns in test.ns %}
Nameserver {{ ns.hostname }} does not contain a record for any of the domains:
{%         for fqdn in ns.fqdn %}    {{ fqdn }}
{%         endfor %}{%         if ns.overfull %}    ...{% endif %}
{%       endfor %}
{%     elif test.name == "authoritative" %}
{%       for ns in test.ns %}
Nameserver {{ ns.hostname }} is not authoritative for domains:
{%         for fqdn in ns.fqdn %}    {{ fqdn }}
{%         endfor %}{%         if ns.overfull %}    ...{% endif %}
{%       endfor %}
{%     elif test.name == "heterogenous" %}
All nameservers in the NS set use the same implementation of DNS server.
{%     elif test.name == "notrecursive" %}
Following nameservers in the NS set are recursive:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "notrecursive4all" %}
Following nameservers in the NS set answered a query recursively:
{%       for ns in test.ns %}    {{ ns.hostname }}
{%       endfor %}
{%     elif test.name == "dnsseckeychase" %}
For the following domains belonging to the NS set, the DNSSEC signature
could not be validated:
{%       for domain in test.ns %}    {{ domain }}
{%       endfor %}
{%     endif %}{%   endif %}{% endfor %}
=====================================================================


Your sincerely
Support of CZ.NIC, the CZ domain registry
{% endblock %}
