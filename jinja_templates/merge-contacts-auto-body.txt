{% extends "b9885448-5dc5-4234-938c-034099971d29.email-base.txt" %}
{% block content %}
Dear Customer,

To simplify the administration and management of contact data in the registry, the following changes have been implemented in accordance with the Domain Name Registration Rules, Article 13.1:

Duplicate contact entries having different identifiers but identical contents have been unified. All duplicate contact details were merged into a single entry carrying the identifier {{ dst_contact_handle }}.

{% if domain_registrant_list %}
Holders were changed for the following domains:
{%   for item in domain_registrant_list %}
    {{ item }}
{%   endfor %}
{% endif %}{% if domain_admin_list %}
Administrative contacts were changed for the following domains:
{%   for item in domain_admin_list %}
    {{ item }}
{%   endfor %}
{% endif %}{% if nsset_tech_list %}
Technical contacts were changed for the following nameserver sets:
{%   for item in nsset_tech_list %}
    {{ item }}
{%   endfor %}
{% endif %}{% if keyset_tech_list %}
Technical contacts were changed for the following key sets:
{%   for item in keyset_tech_list %}
    {{ item }}
{%   endfor %}
{% endif %}{% if removed_list %}
The following duplicate contact entries were removed:
{%   for item in removed_list %}
    {{ item }}
{%   endfor %}
{% endif %}

Yours sincerely
Support of CZ.NIC, the CZ domain registry
{% endblock %}
