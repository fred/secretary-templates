{% extends "b9885448-5dc5-4234-938c-034099971d29.email-base.txt" %}
{% block content %}
Dear customer,

we would like to inform you that your registrar has not extended the registration
of the domain name {{ domain }}. Due to this fact and based on the Domain Name Registration Rules, we are cancelling the registration of this domain name.

If you are interested in the registration of the domain again, please contact any registrar listed
on our pages (https://www.nic.cz/whois/registrars).

Yours sincerely
Support of CZ.NIC, the CZ domain registry
{% endblock %}
