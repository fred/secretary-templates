{% extends "b9885448-5dc5-4234-938c-034099971d29.email-base.txt" %}
{% block content %}
Dear business partners,

Enclosed with this letter, we are sending a tax document for domain name
registration services and the maintenance of domain name records in the period
from {{ fromdate }} to {{ todate }} for the {{ zone }} zone.

Yours sincerely
Support of CZ.NIC, the CZ domain registry
{% endblock %}
