#!/usr/bin/python3
"""Load templates to django-secretary.

Usage: load_templates.py [options] <file>...
       load_templates.py -h | --help

Options:
  -h, --help                show this help message and exit
  --settings SETTINGS       The Python path to a settings module, e.g.
                            "myproject.settings.main". If this isn't provided, the
                            DJANGO_SETTINGS_MODULE environment variable will be
                            used.
"""
import os
from pathlib import Path
from typing import Any, Dict, Optional, Type

import django
from django.apps import apps
from django.conf import settings
from django.core.files.base import ContentFile
from django.db.models.fields.files import FieldFile
from django.db.transaction import atomic
from django.utils.timezone import utc
from docopt import docopt

try:
    import yaml
except ImportError:
    yaml = None


def save_asset(model: Type, fields: dict) -> None:
    """Save an asset."""
    # Collect asset data
    obj = None
    data = {}

    name = fields.get('name')
    if name is None:
        exit("Name is not defined for {}.".format(model.__name__))
    template = fields.get('template')
    if template is not None:
        try:
            data['template'] = model.template.get_queryset().get(**template)
        except model.DoesNotExist:
            exit("Template for {} not found.".format(name))

    for key in ('mime_type', ):
        if key in fields:
            data[key] = fields[key]

    if model.objects.filter(name=name).exists():
        print("Asset {} already exists, skipping.".format(name))
        return
    data['name'] = name

    # Save asset
    obj = model(**data)
    with open(os.path.join('assets', fields.get('content', name)), 'rb') as content:
        obj.content.save(os.path.basename(name), ContentFile(content.read()))
    obj.save()
    print("Asset {} created.".format(name))


def _normalize_content(content: str) -> str:
    return content.strip().replace('\r', '')


def _contents_match(file_content: Optional[str], db_file: FieldFile) -> bool:
    """Check if file content matches the database.

    They should either be both empty, or both contain the same content.
    """
    if db_file:
        db_file.seek(0)
    return ((file_content is not None) == bool(db_file)) and (  # FieldFile is `False` when empty
        (file_content is None) or (
            _normalize_content(file_content) == _normalize_content(db_file.read().decode())))


def save_template(model: Type, fields: dict) -> None:
    """Save a template."""
    obj = None
    data: Dict[str, Any] = {'is_active': True}

    name = fields.get('name')
    if name is None:
        exit("Name is not defined for {}.".format(model.__name__))
    data['name'] = name

    django_template_path = Path('templates') / fields.get('content', name)
    django_template = django_template_path.read_text() if django_template_path.exists() else None
    jinja_template_path = Path('jinja_templates') / fields.get('content', name)
    jinja_template = jinja_template_path.read_text() if jinja_template_path.exists() else None

    uuid = fields.get('uuid')

    # Some templates have specified uuid. Those objects need special treatment: we can't delete them
    # and create a new version, because that would require changing the uuid. So we instead change those in-place.
    if uuid is not None:
        obj = model.objects.filter(uuid=uuid).first()
        if obj is not None:
            changed = False
            if _contents_match(None, obj.content) and django_template is not None:
                obj.content.save(name, ContentFile(django_template))
                obj.save()
                print(f"Django content updated for an existing template {uuid}.")
                changed = True
            if _contents_match(None, obj.jinja_content) and jinja_template is not None:
                obj.jinja_content.save(name, ContentFile(jinja_template))
                obj.save()
                print(f"Jinja content updated for an existing template {uuid}.")
                changed = True
            if not changed:
                print("Template {} already exists, skipping.".format(obj))
            obj.jinja_content.close()
            obj.content.close()
            return
        # if the object doesn't exist, we *can* treat it as every other, just with a pre-defined uuid.
        data['uuid'] = uuid

    parent = fields.get('parent')
    if parent is not None:
        try:
            data['parent'] = model.objects.get(**parent)
        except model.DoesNotExist:
            exit("Parent {} of {} not found.".format(parent, name))
        except model.MultipleObjectsReturned:
            # Search active templates only
            try:
                data['parent'] = model.objects.get(**parent, is_active=True)
            except model.DoesNotExist:
                exit("Parent {} (active) of {} not found.".format(parent, name))
        except model.DoesNotExist:
            exit("Parent of {} not found.".format(name))

    for key in ('create_datetime', 'is_active', 'signer'):
        if key in fields:
            data[key] = fields[key]

    # Try to find object in database.
    assert obj is None
    obj = model.objects.filter(**data).first()
    if obj is not None:
        django_match = _contents_match(django_template, obj.content)
        if django_match and _contents_match(jinja_template, obj.jinja_content):
            # Nothing to update
            obj.content.close()
            obj.jinja_content.close()
            print("Template {} is at the latest version, skipping.".format(obj))
            return
        elif django_match and jinja_template is not None and not obj.jinja_content:
            # If the only thing changed is that Jinja content is *added*, then we don't want to
            # create a new version just for that, so we update the jinja content in-place.
            obj.jinja_content.save(name, ContentFile(jinja_template))
            obj.content.close()
            obj.jinja_content.close()
            obj.save()
            print("Jinja content added to an existing template", obj)
            return
        else:
            # Something changed, needs update. Deactivate the current model, and create a new one with updated contents.
            obj.is_active = False
            obj.save()
            print("Template {} deactivated.".format(obj))
    elif data['is_active']:
        # Object not found. Try to find active template by name only to prevent duplicate problems.
        obj = model.objects.filter(name=data['name'], is_active=True).first()
        if obj is not None:
            # It's a different model. Deactivate it and create a new one below.
            obj.is_active = False
            obj.save()
            print("Template {} deactivated.".format(obj))

    # Save new template
    obj = model(**data)
    if django_template is not None:
        obj.content.save(name, ContentFile(django_template))
    if jinja_template is not None:
        obj.jinja_content.save(name, ContentFile(jinja_template))
    obj.save()
    # Field `create_datetime` is always updated. Force the value if needed.
    if 'create_datetime' in data:
        create_datetime = data['create_datetime']
        if settings.USE_TZ:
            create_datetime = create_datetime.astimezone(utc)
        obj.create_datetime = create_datetime
        obj.save()
    print("Template {} created.".format(obj))


def save_template_collection(model: Type, fields: dict) -> None:
    """Save a template collection."""
    # Collect collection data
    obj = None
    data = {}

    name = fields.get('name')
    if name is None:
        exit("Name is not defined for {}.".format(model.__name__))
    if model.objects.filter(name=name).exists():
        print("Collection {} already exists, skipping.".format(name))
        return
    data['name'] = name
    data['label'] = fields.get('label', name)

    # Save collection
    obj = model(**data)
    obj.save()
    tag_model = obj.tags.model
    for tag_name in fields.get('tags', []):
        tag, _ = tag_model.objects.get_or_create(name=tag_name)
        obj.tags.add(tag)
    print("Collection {} created.".format(name))


def save_obj(data: dict):
    """Save single object."""
    model_name = data.get('model')
    if model_name is None:
        exit("Model is not defined in {}.".format(data))
    model = apps.get_model(model_name)
    fields = data.get('fields')
    if fields is None:
        exit("Fields are not defined for {}.".format(model_name))

    if model_name.lower() == 'django_secretary.asset':
        save_asset(model, fields)
    elif model_name.lower() == 'django_secretary.template':
        save_template(model, fields)
    elif model_name.lower() == 'django_secretary.templatecollection':
        save_template_collection(model, fields)
    else:
        exit("Model {} is not supported.".format(model_name))


def load_file(filename: str):
    """Load and save objects from a file."""
    print("Loading {}".format(filename))
    with atomic():
        with open(filename) as fixture:
            for document in yaml.safe_load_all(fixture):
                for data in document:
                    save_obj(data)


def main():
    options = docopt(__doc__)
    if options['--settings']:
        os.environ['DJANGO_SETTINGS_MODULE'] = options['--settings']
    django.setup()

    if not yaml:
        exit("yaml library not found")

    for filename in options['<file>']:
        load_file(filename)


if __name__ == '__main__':
    main()
