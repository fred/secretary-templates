===================
Secretary-templates
===================

Default set of Fred templates for django-secretary.

Learn more about the project and our community on the [FRED's home page](https://fred.nic.cz).

-----
Usage
-----

This project provides a simple script ``load_templates.py`` to upload templates.
It loads template data from a YAML file with a similar structure to fixtures for the Django ``loaddata`` command.
It should be run with the same settings and python path as the secretary project itself.

Run ``load_templates.py --help`` to see all options.

Every object in the fixture requires a ``model`` and a ``fields`` keys.
Following models are supported:

django_secretary.Template
=========================

Fields must contain a ``name`` of the template and may contain other fields.

``uuid``
--------
The template is skipped if a template with the same ``uuid`` already exists in the database.

``name``
--------
The name of the template.
If the template doesn't have ``uuid``, is active and a template with the same ``name`` exists, existing template is deactivated and a new active template is created instead.

``is_active``
-------------
Whether the template should be active.
By default ``True``.

``parent``
----------
Defines the template parent using its attributes, such as ``uuid`` or ``name``.

``create_datetime``
-------------------
Override template's date and time of a creation.

``signer``
----------
Define template's signer.

``content``
-----------
A name of a file within the ``jinja_templates`` for Jinja version (or ``templates`` directory for Django version) with the template itself.
By default ``name`` is used.

django_secretary.Asset
======================

Fields must contain a ``name`` of the asset and may contain other fields.

``name``
--------
The name of the asset.
The asset is skipped if an asset with the same ``name`` already exists in the database.

``template``
------------
Defines the linked template using its attributes, such as ``uuid`` or ``name``.

``mime_type``
-------------
Define template's MIME type.

``content``
-----------
A name of a file within the ``assets`` directory with the asset itself.
By default ``name`` is used.

django_secretary.TemplateCollection
===================================

Fields must contain a ``name`` of the template collection and may contain other fields.

``name``
--------
The name of the template collection.
The template collection is skipped if a template collection with the same ``name`` already exists in the database.

``label``
---------
Defines the template collection label.
By default ``name`` is used.

``tags``
--------
Define template collection's tags.
New tags are created automatically.

-----------------
Repository layout
-----------------

Jinja templates are stored in ``jinja_templates`` directory (flat structure).
Legacy Django templates are stored in ``templates`` directory (flat structure).
Assets (styles, fonts, images) are stored in ``assets`` directory (tree structure).

Templates' metadata are stored in several project-specific YAML files with two exceptions.

``fred-migration.yml`` contains template versions used in migration for the old template system.
These templates shall not be changed.
If the new version is required, it should be placed in the respective YAML file and respective record in ``fred-migration.yml`` needs to be disabled (``is_active`` set to ``false``).

``pdf-templates.yml`` contains all templates for PDF documents because they share assets, which are not versioned.

--------
Workflow
--------

Apart from templates in ``fred-migration.yml``, repository contains templates in their current production versions.
Changes in templates should be commited to branch.
The branches should be merged to master and tagged just before their deploy to production, i.e. after new templates are tested.

----------------------------
Migration to Jinja templates
----------------------------

Templates are mostly compatible between Jinja and Django version.
One of the main changes is deprecation of ``parent`` template, which is replaced by using template UIDs directly in templates.
UIDs are in form ``{uuid}.{template-name}``.
In Jinja templates this also allows usage of ``include`` tags.

Example of migration of Django template to Jinja template::

   -{% extends template.parent.name %}
   +{% extends "b9885448-5dc5-4234-938c-034099971d29.email-base.txt" %}

In Jinja templates there is no ``load`` template tag, all tags and filters are defined in engine configuration.
In Jinja templates there is no ``localize`` template tag, variables are not localized before output and must be localized by ``localize`` template filter individually where required.
